# README #

>Problem: Google is penalizing slow loading websites.

This solution was developed to load a graphics intensive residential real estate website.  When the site first deployed, the home page took 3 to 4 seconds to load.  After the Python based ninja templating engine had been engaged, the load time dropped to under 225ms.  This was a massive improvement in speed, **load speed increased from 3 - 4 seconds to 0.25 seconds (250ms).**

## Employed JINJA template rendering engine ##

* We needed to maintain the SCSS styling and didn't want to hand code each page's HTML.
* Could have had the system use WordPress's output for input, but wanted to keep it simple.
* This type of technology can be deployed to update at a predetermined time or multiple times per day.
* JSON based menu system
* Cache busting should be employed
* This is CDN friendly

### JINJA template sample ###
```html
{# Page-single.njk #}

<!-- base-layout.njk -->
{% extends data.base_layout %}

{% if not data.page_title %}
  {% set data = {page_title: 'No page title passed...'} %}
{% endif %}

{% block content %}


<div id="page-sidebar-right">
  <article class="main-content" id="page-name">
  <!--  Hero image -->
  <div class="row servant-hero-row">
    <div class="servant-hero-image">
    </div>
  </div>

<!--  Mission panel-->
<div class="row">
  <!--  Used to pad -->
  <div class="mission-title small-12 medium-12 columns">

    <h1>{{ data.hero_title|safe }}</h1>
  </div>
  <div class="mission small-12 medium-12 columns">
    <p>
      {{ data.mission_text|safe }}
      <span class="note">{{ data.mission_note|safe }}</span>
    </p>
  </div>
</div>

<!-- Left Column -->
<div class="row row-front-call-to-action">
  <div class="container-main">
  {% block template %}

  {% endblock %}
  </div>

  </div>



</article>
</div> <!-- End of calls -->

{% endblock%}
```
#### HTML injected content ####
```html
<div class="call-to-action small cta-style-1">
  <h3>Stop Leasing Your Home</h3>
  <hr/>
  <p>Buy a home for yourself, not one for your landlord.  Have a questions?  We
    have answers and want to assist you.  Lets get started today. It won't cost you a thing.
  </p>
  <div class="link">
    <a href="/stop-leasing.html">Common issues</a>
  </div>
</div>
```

### Styling ####
FoundationPress 2.9.x SCSS

### Tools ###
* Ninja
* Gulp
* Bable
