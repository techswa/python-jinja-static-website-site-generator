// Custom slider for front page

var fp_slider, slider;
function init1(){

  // var current_page = top.location.pathname;
  // switch(current_page) {
  //   case '/index.html':
  //     alert('Page = ' + current_page);
  //     break;
  //   case '/home-buying-program.html':
  //     alert('Page = ' + current_page);
  //     break;
  //   default:
  //     alert('Some other page');
  //     break;
  // }


	var fp_slider = document.getElementById('fp-slider');
	var slider = document.getElementById('slider');
  var slide_1 = document.getElementById('slide-1');
  var slide_2 = document.getElementById('slide-2');
	var info = document.getElementById('info');
	fp_slider.addEventListener('mousedown', startSlide, false);
	fp_slider.addEventListener('mouseup', stopSlide, false);
  slide_1.style.display = "block";
  slide_2.style.display = 'block';
  // document.getElementById('slide-1').style.display = 'inline-block';
}
function startSlide(event){
	var set_perc = ((((event.clientX - fp_slider.offsetLeft) / fp_slider.offsetWidth)).toFixed(2));
	info.innerHTML = 'start' + set_perc + '%';
	fp_slider.addEventListener('mousemove', moveSlide, false);
	slider.style.width = (set_perc * 100) + '%';
}
function moveSlide(event){
	var set_perc = ((((event.clientX - fp_slider.offsetLeft) / fp_slider.offsetWidth)).toFixed(2));
	info.innerHTML = 'moving : ' + set_perc + '%';
	slider.style.width = (set_perc * 100) + '%';
}
function stopSlide(event){
	var set_perc = ((((event.clientX - fp_slider.offsetLeft) / fp_slider.offsetWidth)).toFixed(2));
	info.innerHTML = 'done : ' + set_perc + '%';
	fp_slider.removeEventListener('mousemove', moveSlide, false);
	slider.style.width = (set_perc * 100) + '%';
}
