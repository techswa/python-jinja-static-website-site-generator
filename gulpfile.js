/*jslint node: true */
"use strict";

var gulp        = require('gulp');
var $           = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var argv        = require('yargs').argv;
var gulpRevAll  = require('gulp-rev-all'); //cache bust rev creator
var gulpData    = require('gulp-data');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload; //Added by BB
var merge       = require('merge-stream');
var sequence    = require('run-sequence');
var colors      = require('colors');
var dateFormat  = require('dateformat');
var del         = require('del');
var newer       = require('gulp-newer');
var nunjucksRender = require('gulp-nunjucks-render');
var sass = require('gulp-sass');
var data = require('gulp-data');
var htmlclean = require('gulp-htmlclean')
var cleanCSS    = require('gulp-clean-css');
var gulpFileLog = require('gulp-filelog');
var gutil = require('gulp-util');

// Set to === if production
var devBuild = (process.env.NODE_ENV === 'production');
var folder = {
    src: 'assets/',
    output: 'assets/'
};

//Tutorial used for html and newer
//https://www.sitepoint.com/introduction-gulp-js/

// Deployment
//https://www.npmjs.com/package/gulp-ssh
var fs = require('fs');
// var gulp = require('gulp');
var GulpSSH = require('gulp-ssh');
var config = {
  host: 'swapp.webfactional.com',
  port: 22,
  username: 'swapp',
  privateKey: fs.readFileSync('/Users/swa/.ssh/id_rsa')
}

var gulpSSH = new GulpSSH({
    ignoreErrors: false,
    sshConfig: config
})

//Deploy tasks
gulp.task('sftp-html-top', function () {
  return gulp.src('/users/swa/sites/*.html')
    .pipe(gulpSSH.dest('/home/swapp/webapps/ccr_wp/'))
})

gulp.task('sftp-html-lists', function () {
  return gulp.src('/users/swa/sites/lists/*.html')
    .pipe(gulpSSH.dest('/home/swapp/webapps/ccr_wp/lists/'))
})

gulp.task('sftp-html-loans', function () {
  return gulp.src('/users/swa/sites/loan-programs/*.html')
    .pipe(gulpSSH.dest('/home/swapp/webapps/ccr_wp/loan-programs/'))
})

// Deploty CSS
gulp.task('sftp-css', function () {
  return gulp.src('/users/swa/sites/wp-content/themes/foundationpress/assets/stylesheets/foundation.css')
    .pipe(gulpSSH.sftp('write', '/home/swapp/webapps/ccr_wp/wp-content/themes/foundationpress/assets/stylesheets/foundation.css'))
})


// Enter URL of your local server here
// Example: 'http://localwebsite.dev'
// var URL = 'localhost/test.html';
var URL = 'localhost/index.html';

// Check for --production flag
var isProduction = !!(argv.production);
// Need to find and normalize all isProduction checks

// Browsers to target when prefixing CSS.
var COMPATIBILITY = [
  'last 2 versions',
  'ie >= 9',
  'Android >= 2.3'
];

// File paths to various assets are defined here.
var PATHS = {
  sass: [
    // 'assets/components/foundation-sites/scss',
    // 'assets/components/motion-ui/src',
    // 'assets/components/fontawesome/scss',
  ],
  javascript: [
    'assets/components/what-input/what-input.js',
    'assets/components/foundation-sites/js/foundation.core.js',
    'assets/components/foundation-sites/js/foundation.util.*.js',

    // Paths to individual JS components defined below
    'assets/components/foundation-sites/js/foundation.abide.js',
    'assets/components/foundation-sites/js/foundation.accordion.js',
    'assets/components/foundation-sites/js/foundation.accordionMenu.js',
    'assets/components/foundation-sites/js/foundation.drilldown.js',
    'assets/components/foundation-sites/js/foundation.dropdown.js',
    'assets/components/foundation-sites/js/foundation.dropdownMenu.js',
    'assets/components/foundation-sites/js/foundation.equalizer.js',
    'assets/components/foundation-sites/js/foundation.interchange.js',
    'assets/components/foundation-sites/js/foundation.magellan.js',
    'assets/components/foundation-sites/js/foundation.offcanvas.js',
    'assets/components/foundation-sites/js/foundation.orbit.js',
    'assets/components/foundation-sites/js/foundation.responsiveMenu.js',
    'assets/components/foundation-sites/js/foundation.responsiveToggle.js',
    'assets/components/foundation-sites/js/foundation.reveal.js',
    'assets/components/foundation-sites/js/foundation.slider.js',
    'assets/components/foundation-sites/js/foundation.sticky.js',
    'assets/components/foundation-sites/js/foundation.tabs.js',
    'assets/components/foundation-sites/js/foundation.toggler.js',
    'assets/components/foundation-sites/js/foundation.tooltip.js',

    // Motion UI
    'assets/components/motion-ui/motion-ui.js',

    // Include your own custom scripts (located in the custom folder)
    'assets/javascript/custom/*.js',
  ],
  phpcs: [
    '**/*.php',
    '!wpcs',
    '!wpcs/**',
  ],
  pkg: [
    '**/*',
    '!**/node_modules/**',
    '!**/components/**',
    '!**/scss/**',
    '!**/bower.json',
    '!**/gulpfile.js',
    '!**/package.json',
    '!**/composer.json',
    '!**/composer.lock',
    '!**/codesniffer.ruleset.xml',
    '!**/packaged/*',
  ]
};






// Nunjunks templating engine
gulp.task('nunjucksRender', function() {
  //Pickup assests
  var css_manifest = JSON.parse(fs.readFileSync('assets/stylesheets/rev-manifest.json'));
  var js_manifest = JSON.parse(fs.readFileSync('assets/javascript/rev-manifest.json'));
  // var manifestFile = css_manifest + "," + js_manifest ;
  gutil.log(css_manifest);
  gutil.log(js_manifest);
// gutil.log(manifestFile);

  var nunjucksOptions = {
    path: ['njk/templates'],
    watch:true,
    data: {css_manifest, js_manifest}
    // data: manifestFile,
    // manageEnv:function(env){
    //     var data = JSON.parse(fs.readFileSync('assets/stylesheets/rev-manifest.json'));
    //     env.addGlobal('data_manifest',data);
    // }
  }

  // Gets .html and .nunjucks files in pages
  gutil.log(nunjucksOptions)
  return gulp.src('njk/source/**/*.+(html|njk)')
  // Renders template with nunjucks
  .pipe(nunjucksRender(nunjucksOptions))
  // output files in app folder
  .pipe(gulp.dest('njk/html'))
  //Place file in local ~/sites/ - otherwise styles & js paths break
  .pipe(gulp.dest('../../../'))
  .pipe(browserSync.stream());
});


// Browsersync task
gulp.task('browser-sync', ['build'], function() {

  var files = [
            '**/*.php',
            'assets/images/**/*.{png,jpg,gif}',
          ];

  browserSync.init(files, {
    // Proxy address
    proxy: URL,

    // Port #
    // port: PORT
  });
});

// Compile & compress CSS
gulp.task('sass', function() {
  return gulp.src('assets/scss/foundation.scss')
    // .pipe(gulpRevAll.revision()) //Hash file name
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths: PATHS.sass
    }))
    .on('error', $.notify.onError({
        message: "<%= error.message %>",
        title: "Sass Error"
    }))
    .pipe($.autoprefixer({
      browsers: COMPATIBILITY
    }))
    // Minify CSS if run with --production flag
    .pipe($.if(isProduction, cleanCSS()))
    .pipe($.if(!isProduction, $.sourcemaps.write('.')))
    .pipe(gulp.dest('assets/stylesheets'))
    .pipe(gulpRevAll.revision(['.css'])) //Hash file name
    .pipe(gulpFileLog())
    .pipe(gulp.dest('assets/stylesheets'))
    .pipe(gulpRevAll.versionFile())
    // .pipe(gulpRevAll.fileNameManifest('assets/stylesheets/rev-css-manifest.json'))
    .pipe(gulp.dest('assets/stylesheets'))
    .pipe(gulpRevAll.manifestFile())
    .pipe(gulp.dest('assets/stylesheets'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

// Lint all JS files in custom directory
gulp.task('lint', function() {
  return gulp.src('assets/javascript/custom/*.js')
    .pipe($.jshint())
    .pipe($.notify(function (file) {
      if (file.jshint.success) {
        return false;
      }

      var errors = file.jshint.results.map(function (data) {
        if (data.error) {
          return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
        }
      }).join("\n");
      return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
    }));
});

// Combine JavaScript into one file
// In production, the file is minified
gulp.task('javascript', function() {
  var uglify = $.uglify()
    .on('error', $.notify.onError({
      message: "<%= error.message %>",
      title: "Uglify JS Error"
    }));

  return gulp.src(PATHS.javascript)
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.concat('foundation.js', {
      newLine:'\n;'
    }))
    .pipe($.if(isProduction, uglify))
    .pipe($.if(!isProduction, $.sourcemaps.write()))
    .pipe(gulp.dest('assets/javascript'))

    .pipe(gulpRevAll.revision(['.js'])) //Hash file name
    .pipe(gulp.dest('assets/javascript'))
    .pipe(gulpRevAll.versionFile())
    .pipe(gulp.dest('assets/javascript'))
    .pipe(gulpRevAll.manifestFile())
    .pipe(gulp.dest('assets/javascript'))

    .pipe(browserSync.stream());
});


//Used for HTML to read JSON file
function getDataForFile() {
    //  return JSON.parse(fs.readFileSync('./examples/' + path.basename(file.path) + '.json'));
    return JSON.parse(fs.readFileSync('assets/stylesheets/' + 'rev-manifest.json'));
  };

//Minify HTML files
gulp.task('html', function(){
 var out = 'html/';
 var page = gulp.src('html/**/*');
      // .pipe(newer(out));
// minify production code
var devBuild = true;
if (!devBuild) {
  page = page.pipe(htmlclean());
}
// var mydata = getDataForFile();
// gutil.log(mydata);

return page.pipe()
.pipe(gulp.dest(out))
});

// Copy task
gulp.task('copy', function() {
  // Font Awesome
  var fontAwesome = gulp.src('assets/components/fontawesome/fonts/**/*.*')
      .pipe(gulp.dest('assets/fonts'));

  return merge(fontAwesome);
});

// Build task
// Runs copy then runs sass & javascript in parallel
gulp.task('build', ['clean'], function(done) {
  sequence('copy',
          ['sass', 'javascript', 'lint'],
          done);
});


// Clean task
gulp.task('clean', function(done) {
  sequence(['clean:javascript', 'clean:css'],
            done);
});

// Clean JS
gulp.task('clean:javascript', function() {
  return del([
      'assets/javascript/foundation.js',
      'assets/javascript/foundation.min.js'
      // 'assets/javascript/foundation.*.js'
    ]);
});

// Clean CSS
gulp.task('clean:css', function() {
  // console.log('Entering clean css');
  return del([
      'assets/stylesheets/foundation.css',
      'assets/stylesheets/foundation.css.map',
      'assets/stylesheets/*.css',
      'assets/stylesheets/*.map',
    ]);
});

// Default gulp task
// Run build task and watch for file changes
gulp.task('default', ['build', 'browser-sync'], function() {
  // Log file changes to console
  function logFileChange(event) {
    var fileName = require('path').relative(__dirname, event.path);
    console.log('[' + 'WATCH'.green + '] ' + fileName.magenta + ' was ' + event.type + ', running tasks...');
  }

  gulp.task('style-sheet-compile', function(callback) {
    runSequence('clean:css'
                ,'sass'
                , 'nunjucksRender'
                , callback);
  });

  gulp.task('javascript-compile', function(callback) {
    runSequence('clean:javascript'
                , 'javascript'
                , 'lint'
                , 'nunjucksRender'
                , callback);
  });

  gulp.watch(['assets/scss/**/*.scss'], ['style-sheet-compile'])
      .on('change', function(event) {
        logFileChange(event);
      });

  // JS Watch
  gulp.watch(['assets/javascript/custom/**/*.js'], ['javascript-compile'])
    .on('change', function(event) {
      logFileChange(event);
    });

    // Nunjuck Watch
    gulp.watch(['njk/**/*.njk'], ['nunjucksRender'])
    .on('change', function(event) {
      logFileChange(event);
    });

});
